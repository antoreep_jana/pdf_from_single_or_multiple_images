# PDF_from_Single_or_Multiple_Images

Generate a pdf corresponding to each input image from the input images
or
generate a combined pdf from all the input images.


### Make sure that you have python3 installed in your system before running the script.


Place the python file in the images folder and in command line run

    1) "python3 pdf_from_multi_images.py" if your system has both python 2 and python3 installed
    2) "python pdf_from_multi_images.py" if your system contains only python3

If you have PyPDF2 alredy installed, good enough else when the script runs select your operating system and PyPDF2 will be 
installed. (Linux and Windows)

Can also be used to convert notes in images format to pdf format in Whatsapp